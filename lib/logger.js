import path from 'path';
import { createLogger, format, transports } from 'winston';

const { label, combine, colorize, printf, timestamp } = format;

const NODE_ENV = process.env['NODE_ENV'];
const PKG_NAME = process.env['npm_package_name'];
const PKG_VERSION = process.env['npm_package_version'];

const myFormat = printf(({ level, label, message, timestamp }) => {
  return `${level}: ${label} - ${timestamp} -> ${message}`;
});

const logger = createLogger({
  level: 'info',

  format: combine(
    label({ label: `${PKG_NAME} v:${PKG_VERSION}` }),
    timestamp(),
    myFormat
  )
});

logger.add(new transports.File({ filename: path.join('log', `error.log`), level: 'error' }));
logger.add(new transports.File({ filename: path.join('log', `combined.log`), level: 'info' }));

if (NODE_ENV !== 'test') {
  logger.add(
    new transports.Console({
      format: combine(
        colorize(),
        myFormat
      )
    }));
}

export default logger;

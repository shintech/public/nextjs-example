export default {
  main: [
    { title: `home`, path: `/` },
    { title: `about`, path: `/about` },
    { title: `contact`, path: `/contact` }
  ],

  admin: [
    { title: `pages`, path: `/admin/pages` },
    { title: `admin`, path: `/admin` }
    // { title: `blog`, path: `/blog` }
  ]
};

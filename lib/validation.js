const validate = (key, value) => {
  if (value.trim() === '') {
    return `${key} is required!`;
  }

  if (key === 'username' || key === 'password') {
    return null;
  }

  if ((key !== 'message' && key !== 'email') && /[^a-zA-Z -]/.test(value)) {
    return 'Invalid characters!';
  }

  const length = (key === 'message') ? 10 : 3;

  if (value.trim().length < length) {
    return `${key} needs to be at least ${length} characters!`;
  }

  if (key === 'email') {
    const regex = RegExp(/^(([^<>()[\].,:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i);

    if (!regex.test(value.trim())) {
      return 'Please enter a valid email!';
    }
  }

  return null;
};

export default validate;

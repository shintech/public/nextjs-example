import { useMemo } from 'react';
import { ApolloClient, InMemoryCache, from } from '@apollo/client';
import { createPersistedQueryLink } from '@apollo/client/link/persisted-queries';
import { onError } from '@apollo/client/link/error';
import { sha256 } from 'crypto-hash';
import fetch from 'isomorphic-unfetch';

let apolloClient;
const uri = (process.env['NODE_ENV'] !== 'production') ? 'https://dev.shintech.ninja/api/graphql' : 'https://www.tulsatechservices.com/api/graphql';

function createIsomorphLink () {
  const { HttpLink } = require('@apollo/client/link/http');

  return new HttpLink({
    uri,
    credentials: 'same-origin',
    fetch
  });
}

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.forEach(({ message, locations, path }) =>
      // eslint-disable-next-line no-console
      console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
    );
  }

  // eslint-disable-next-line no-console
  if (networkError) console.log(`[Network error]: ${networkError}`);
});

function createApolloClient () {
  const persistedQueriesLink = createPersistedQueryLink({ sha256 });

  return new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link: from([errorLink, persistedQueriesLink, createIsomorphLink()]),
    cache: new InMemoryCache()
  });
}

export function initializeApollo (initialState = null) {
  const _apolloClient = apolloClient ?? createApolloClient();

  if (initialState) {
    _apolloClient.cache.restore(initialState);
  }

  if (typeof window === 'undefined') return _apolloClient;
  if (!apolloClient) apolloClient = _apolloClient;

  return _apolloClient;
}

export function useApollo (initialState) {
  const store = useMemo(() => initializeApollo(initialState), [initialState]);
  return store;
}

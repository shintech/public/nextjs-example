import { makeVar } from '@apollo/client';

export const snackbarVar = makeVar({
  open: false,
  message: '',
  severity: 'info'
});

import http from 'https';

const secret = process.env['RECAPTCHA_SECRET'];

const request = (url, response) => new Promise((resolve, reject) => {
  const { hostname, pathname } = new URL(url);

  const options = {
    hostname,
    path: pathname,
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };

  let string = '';

  const req = http.request(options, res => {
    res.on('data', chunk => {
      string += chunk;
    });

    res.on('end', () => {
      resolve(JSON.parse(string));
    });
  });

  req.on('error', reject);

  req.write(`secret=${secret}&response=${response}`);
  req.end();
});

export default request;

import nodemailer from 'nodemailer';

const smtpHost = process.env['SMTP_HOST'];
const smtpPort = process.env['SMTP_PORT'] || 587;
const smtpSender = process.env['SMTP_SENDER'];
const smtpPass = process.env['SMTP_PASS'];
const smtpBCC = process.env['SMTP_BCC'] || smtpSender;

const sendMail = (message) => new Promise(function (resolve, reject) {
  const transporter = nodemailer.createTransport({
    host: smtpHost,
    port: smtpPort,
    // secure: true,
    auth: {
      user: smtpSender,
      pass: smtpPass
    }
  });

  transporter.sendMail({
    ...message,
    from: smtpSender,
    bcc: smtpBCC,
    replyTo: message.to
  }, (error, info) => {
    if (error) {
      return reject(error);
    }

    resolve(`Message sent: ${info.response} -> from: ${smtpSender} - bcc: ${smtpBCC} - to: ${message.to}`);
  });
});

export default sendMail;

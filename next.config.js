const { PHASE_DEVELOPMENT_SERVER } = require('next/constants');
// const status = process.env['npm_package_config_status']

module.exports = (phase) => {
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    const eslintLoader = {
      enforce: 'pre',
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'eslint-loader',
      options: {
        failOnWarning: false,
        failOnError: false,
        emitWarning: true
      }
    };

    return {
      webpack: (config) => {
        config.plugins = config.plugins || [];

        config.module.rules = [
          ...config.module.rules,
          eslintLoader
        ];

        return config;
      }
    };
  }

  return {
    webpack: (config) => {
      config.plugins = config.plugins || [];

      config.module.rules = [
        ...config.module.rules
      ];

      return config;
    }
  };
};

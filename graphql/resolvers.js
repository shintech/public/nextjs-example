import { readdir } from 'fs/promises';
import path from 'path';
import { ObjectID } from 'mongodb';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import request from '../lib/request';
import sendMail from '../lib/mail';
import logger from '../lib/logger';

const JWT_SECRET = process.env['JWT_SECRET'] || 'JWT_SECRET';

export default {
  Query: {
    posts (_, _args, _context, _info) {
      return _context.db
        .collection('posts')
        .find()
        .toArray()
        .then((posts) => posts);
    },

    post (_, _args, _context, _info) {
      return _context.db
        .collection('posts')
        .findOne({ _id: new ObjectID(_args.id) })
        .then((post) => post);
    },

    products (_, _args, _context, _info) {
      return _context.db
        .collection('products')
        .find()
        .toArray()
        .then((products) => products);
    },

    product (_, _args, _context, _info) {
      return _context.db
        .collection('pages')
        .findOne({ slug: 'home' })
        .then(page => page.products.find(product => product.slug === _args.slug));
    },

    pages (_, _args, _context, _info) {
      return _context.db
        .collection('pages')
        .find()
        .toArray()
        .then((pages) => pages);
    },

    page (_, _args, _context, _info) {
      return _context.db
        .collection('pages')
        .findOne({ slug: _args.slug })
        .then((page) => page);
    },

    async me (_, _args, _context, _info) {
      const findUser = (username) => new Promise((resolve, reject) => {
        _context.db.collection('users').findOne({ username }, (err, res) => {
          if (err) return reject(err);
          resolve(res);
        });
      });

      return _context.user?.username ? findUser(_context.user.username) : null;
    },

    async files (_, _args, _context, _info) {
      const filePath = path.join(process.cwd(), 'public/images');
      const files = await readdir(filePath);

      return files.map(file => ({ filename: file }));
    }
  },

  Mutation: {
    async createEmail (_, _args, _context, _info) {
      try {
        const response = await request('https://www.google.com/recaptcha/api/siteverify', _context.headers['g-recaptcha-response']);

        if (response.success) {
          const message = {
            to: _args.email,
            subject: `Thank you for contacting us...`,
            html: `<div>
              <h1>Thank you for contacting us...</h1>
              <h2>We will contact you momentarily...</h2>
              <p>Below is a summary of your submission...</p>
              <ul>
                <li>Name: ${_args.name}</li>
                <li>Email: ${_args.email}</li>
                <li>Message: ${_args.message}</li>
              </ul>
            </div>`
          };

          const mailResponse = await sendMail(message);

          logger.info(mailResponse);

          return {
            message: mailResponse
          };
        }
      } catch (err) {
        logger.info(err.message);
      }
    },

    async page (_, _args, _context, _info) {
      if (!_context.user?._id) { throw new Error('AuthenticationFailed'); }

      try {
        const pages = _context.db.collection('pages');

        const filter = {
          slug: _args.slug
        };

        const updateDoc = {
          $set: {
            title: _args.title,
            content: _args.content,
            image: _args.image
          }
        };

        const options = { returnNewDocument: true };

        const page = await pages.findOneAndUpdate(filter, updateDoc, options);

        logger.info(`updated page id: ${page.value.slug}...`);

        return page.value;
      } catch (err) {
        logger.error(err);

        throw new Error(err.message);
      }
    },

    async authenticate (_, _args, _context, _info) {
      try {
        const response = await request('https://www.google.com/recaptcha/api/siteverify', _context.headers['g-recaptcha-response']);

        if (!response.success) {
          for (let error of response['error-codes']) {
            logger.error(`[Error]: reCAPTCHA -> ${error}...`);
          }

          throw new Error('AuthenticationFailed');
        }

        const user = await _context.db.collection('users').findOne({ username: _args.username });

        if (!user) { throw new Error('AuthenticationFailed'); }

        const authenticated = await bcrypt.compare(_args.password, user.password);

        if (!authenticated) { throw new Error('AuthenticationFailed'); }

        const generateWebToken = ({ _id, username }) =>
          jwt.sign({ _id, username }, JWT_SECRET, {
            expiresIn: 60 * 60 * 3600
          });

        const token = generateWebToken(user);

        _context.cookies.set('auth-token', token, {
          httpOnly: true,
          sameSite: 'lax',
          maxAge: 60 * 60 * 3600,
          secure: false,
          overwrite: true
        });

        return {
          message: `Authentication Success`,
          status: 'success',
          user: {
            username: _args.username,
            token
          }
        };
      } catch (err) {
        logger.error(`[${err.message}]: username: ${_args.username} - ip: ${_context.headers['x-real-ip']}`);

        return {
          status: 'error',
          message: 'Authentication Failed'
        };
      }
    }
  }
};

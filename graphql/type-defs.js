import { gql } from '@apollo/client';

export default gql`
  type Post {
    _id: ID!
    title: String
    content: String
  }

  type Section {
    title: String
    content: String
    description: String
    image: String
  }

  type Email {
    message: String
    name: String
    email: String
  }

  type Link {
    name: String
    message: String
    href: String
    color: String
  }

  type Page {
    _id: ID!
    slug: String!
    title: String
    content: String
    image: String
    sections: [Section]
    properties: [[String]]
    links: [Link]
    products: [Product]
  }

  type Product {
    _id: ID!
    title: String!
    slug: String!
    description: String!
    content: String!
    image: String!
  }

  type User {
    _id: ID!
    username: String!
    token: String!
  }

  type AuthenticationResponse {
    status: String
    message: String!
    user: User
  }

  type File {
    filename: String!
  }

  input PageInput {
    "The title of the page"
    title: String!

    "A description of the page"
    description: String!

    "The content of the page"
    content: String!

    "The name image"
    image: String!

    "The slug should be changed with caution"
    slug: String
  }
  
  type Query {
    me: User

    posts: [Post]
    post(id: String!): Post!

    pages: [Page]
    page(slug: String!): Page!

    products: [Product]
    product(slug: String!): Product!

    files: [File]
  }

  type Mutation {
    createEmail(email: String, name: String, message: String): Email
    authenticate(username: String!, password: String!): AuthenticationResponse
    page(title: String, content: String, image: String, slug: String): Page
  }
`;

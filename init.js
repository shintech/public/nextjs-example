const properties = [
  ['phone', '5555555555'],
  ['address', '123 St Address, City, ST, 12345'],
  ['email', 'test@example.org'],
  ['sitename', 'Tulsa Tech Services']
];

const products = [
  {
    title: 'Remote Support Services',
    slug: 'rustic-plastic-pants',
    description: 'Sit qui veritatis dolore nihil quis alias odio. Id non est deserunt. Nihil vel dignissimos non delectus aut quibusdam. Dicta ad non blanditiis molestias itaque sed. Exercitationem veniam soluta repellendus non.',
    content: 'Tempora ea maxime consequatur omnis. Similique et at. Recusandae corrupti maiores sed. Saepe excepturi quia doloribus quo. Dicta excepturi delectus est ipsa. Tempore aut quidem minima sint quia animi quia quas modi. Atque modi ut velit doloribus. Ipsam et est repudiandae ut est omnis iusto consequatur. Optio beatae suscipit qui itaque ut aut veritatis. Natus occaecati iure sed quidem. Enim illo et ex labore deserunt eius facere. Animi aut labore. Qui nihil architecto. Ut voluptatem optio et quasi quia culpa quia.',
    image: 'developer.svg'
  },

  {
    title: 'Virus and Spyware Removal',
    slug: 'practical-concrete-chair',
    description: 'Aliquid odio repellat totam qui iusto. Distinctio sequi quae.',
    content: 'Asperiores tempora eius voluptatem velit sunt et et. Nostrum saepe consequuntur ad. Quo velit id iste aliquam assumenda vero. Suscipit assumenda sed nulla et repudiandae aut tenetur mollitia. Vel modi velit recusandae quia minima illo quia dolor. Consequuntur voluptas libero aut iure similique non ab molestiae. Non similique alias unde quis. Adipisci eum nisi animi enim aspernatur explicabo laboriosam doloremque voluptas. Reprehenderit et excepturi distinctio expedita temporibus eius. Voluptas tempora qui. Debitis ut numquam facilis quis. Rerum magnam commodi quam fuga et eveniet quo in et.',
    image: 'building.svg'
  },

  {
    title: 'Business IT Consulting',
    slug: 'ergonomic-metal-chair',
    description: 'Illo alias nulla maxime dolor velit nam voluptatem sit similique. Debitis dolor aut ea sunt. Iste consectetur odit et voluptas autem omnis. Consequatur architecto esse quis et sapiente illo consectetur et.',
    content: 'Facilis tempore unde quia qui ipsam nihil corporis. Neque aut est nemo nam laborum accusamus placeat eligendi. Odio et odit ut enim et qui. Et dolorem est nostrum repudiandae magnam molestias. Ducimus neque ad voluptatem. Praesentium rerum nesciunt veniam est. Corrupti neque necessitatibus facilis. Quaerat ipsa neque molestiae ab rerum. Assumenda quaerat beatae dolor quia maxime. Et harum aperiam provident. Earum blanditiis provident quae occaecati molestiae aperiam et ut. Ut omnis est.',
    image: 'analytics.svg'
  },

  {
    title: 'Managed IT Services',
    slug: 'practical-rubber-keyboard',
    description: 'Quia dolorem architecto velit delectus quia. Maxime sit maxime suscipit qui optio. Deserunt ratione pariatur et earum inventore molestiae. At qui consequatur consequatur autem occaecati molestiae temporibus aperiam sit.',
    content: 'Ut accusamus dicta et. Consectetur ut minima ut. Soluta autem vel enim eos accusantium dolorem. Iure dolore beatae adipisci officia. Laboriosam soluta ut temporibus fugiat aliquam nisi alias. Quis vitae fugit eos. Laudantium eius non voluptas. Laudantium fugit vero. Facilis quod laborum ut quidem aut quasi ipsam rem. Asperiores vel vel culpa laborum animi placeat quia iusto deleniti. Itaque exercitationem rem laboriosam et velit. Quisquam neque quas. Placeat et vel adipisci fugiat alias. Aut eum sunt error. Ducimus laudantium incidunt consequatur nam quaerat accusantium nulla aspernatur. Fugit et hic quas et.',
    image: 'world.svg'
  }
];

const pages = [
  {
    title: 'Tulsa Tech Services',
    description: 'Home Page',
    content: '---\nAlias amet tempore odio. Autem sit qui quod quia quo temporibus. Aut ad eum nulla earum similique qui sed qui. Officia esse recusandae est aliquam earum numquam porro qui. Nobis quam voluptatem. Explicabo consequatur officiis et illum rerum dolores non maiores. Aperiam quis quisquam sit veniam. Dolor eaque ut quis accusamus. Nesciunt quisquam eum et qui. Exercitationem deserunt nihil officiis fugit nihil dicta. Rem dolore quidem blanditiis quas fugiat quaerat facilis at esse. Vel autem quos excepturi voluptatem aut autem occaecati sit et. Esse architecto rerum. Tempora repellendus repellat voluptatem porro dolor ut doloremque.',
    links: [
      {
        name: 'about',
        message: 'About Us',
        href: '/about'
      },

      {
        name: 'contact',
        message: 'Schedule a Consultation',
        href: '/contact',
        color: 'primary'
      }
    ],
    slug: 'home',
    image: 'moon.svg',
    sections: [],
    properties,
    products
  },
  {
    title: 'About Us',
    description: 'About Page',
    content: '---\nEst nostrum ut dolorem voluptas neque aliquam accusamus odio sit. Accusantium optio at dignissimos quis dolor molestiae impedit pariatur illo. Dolores sint dicta cumque velit est assumenda commodi quos. Nihil adipisci est minima laboriosam minima dolor cumque nulla assumenda. Qui quia minima. Impedit veritatis eum dolorem et neque. A odit aut nulla repellat odio non et facilis repudiandae. Voluptatem consequatur placeat. Vitae eligendi ex. Porro vero beatae commodi. Et aperiam quia maxime nemo dolores officiis vitae necessitatibus eius. Iste eaque sint tenetur rem aspernatur rem ut quae et. Vel omnis ut eaque qui adipisci recusandae. Aperiam eum tempore itaque quod amet dignissimos.',
    slug: 'about',
    image: 'hologram.svg',
    properties,
    sections: [
      {
        title: 'Array',
        description: 'description',
        image: 'hologram.svg',
        content: 'Quibusdam provident debitis illum dolores quia aliquam. Cum alias dignissimos consequatur laborum sed consequatur eveniet. Provident labore libero quasi assumenda suscipit et totam. Soluta est quia quo excepturi quaerat. Natus sequi aut eum. Ut ea corporis sit exercitationem aliquam ut dignissimos in aperiam. Sed eum quidem natus. Doloremque provident cupiditate voluptas qui natus unde. Sunt totam quia nobis. Incidunt reprehenderit enim nulla nulla. Sit autem corporis sed soluta odio aut quam. Possimus veniam quaerat accusantium dolor voluptate itaque a. Excepturi facilis asperiores eius ut beatae voluptas. Nostrum non necessitatibus dolor quia. Mollitia quis recusandae rerum iure. Sequi molestias autem delectus eaque corrupti quae autem inventore. Rerum repellat cum veritatis molestias. Optio exercitationem quis corrupti.'
      },
      {
        title: 'Firewall',
        description: 'description',
        image: 'progressive.svg',
        content: 'Ad eius soluta. Id eveniet ex voluptatem maiores possimus sit explicabo qui eum. Debitis beatae labore repudiandae nostrum quisquam. At sit velit velit rerum labore officia vero hic. Asperiores deserunt unde quia tenetur est et impedit cumque. Mollitia voluptates quos labore et quia assumenda rerum consequatur delectus. Ab earum eos sed adipisci eveniet qui similique. Eum nulla nostrum reprehenderit porro numquam rem est at rerum. Ut aspernatur fugiat omnis recusandae quo est. Laudantium molestias nihil quos et. Commodi porro est in labore earum deleniti pariatur sequi odio. Aspernatur quia qui rerum. Pariatur voluptas voluptas autem molestias placeat doloribus laborum. Voluptates fugit voluptatem sit enim in nihil. Adipisci cumque totam quia doloremque quidem est quia. Et sed quis ea rerum optio. Doloremque ipsum qui placeat sint assumenda unde facere laudantium maxime.'
      },
      {
        title: 'Application',
        description: 'description',
        image: 'worktime.svg',
        content: 'Itaque nihil eos consequatur fuga et magni. Eligendi impedit est voluptatem dolores. Delectus qui dolore facilis dignissimos enim aut harum reiciendis molestias. Et doloremque ut ex necessitatibus aut necessitatibus rerum quo rerum. Maxime voluptas rerum expedita et quisquam quidem est. Error dolor perspiciatis quibusdam. Ut illo tempora praesentium quaerat amet. Et dolorum ab est id voluptatem eius. Amet blanditiis est. Nulla dolorem quia perferendis. Nostrum perspiciatis quia animi maxime et. Veniam sint placeat illo repellendus id consequuntur neque et iste. Possimus ut incidunt et. Veritatis illum suscipit in molestiae iure. Dolores animi porro natus debitis inventore.'
      }
    ]
  },
  {
    title: 'Contact Us',
    description: 'Contact Page',
    content: '---\nSequi debitis ut molestias vel inventore ab. Est et quia. Aspernatur quia nobis consequatur saepe. Distinctio modi voluptatibus a ad blanditiis est sapiente aut reprehenderit. Soluta earum doloribus voluptatem eveniet aut sed numquam iste. Enim error id aut eaque id ratione ratione enim. Consequatur ullam tempore ut. Odio eaque qui vitae accusamus accusantium et commodi. Voluptatem eius temporibus veniam voluptate at. Cupiditate fugiat ad soluta earum eligendi excepturi blanditiis. Recusandae et doloremque. Ex voluptatem harum eius ab perspiciatis praesentium. Numquam deleniti et a natus quo non neque. Accusamus dolores ipsa. In sed minima.',
    slug: 'contact',
    image: 'contact.svg',
    sections: [],
    properties
  },
  {
    title: '',
    description: 'Product Page',
    content: '',
    slug: 'product',
    image: 'icon.svg',
    sections: [],
    properties
  },
  {
    title: '',
    description: 'Product Page',
    content: '',
    slug: 'notfound',
    image: 'icon.svg',
    sections: [],
    properties
  },
  {
    title: 'Admin',
    description: 'Admin Page',
    content: 'Please login',
    slug: 'admin',
    image: '',
    sections: [],
    properties
  },
  {
    title: 'Blog',
    description: 'description',
    content: 'content',
    slug: 'blog',
    image: '/images/icon.svg'
  }
];

db.createUser( // eslint-disable-line
  {
    user: 'admin',
    pwd: 'password',
    roles: [
      {
        role: 'readWrite',
        db: 'production'
      }
    ]
  }
);

db.pages.insertMany(pages) // eslint-disable-line

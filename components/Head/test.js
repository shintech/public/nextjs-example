/* eslint-env jest */

import React from 'react';
import { shallow } from 'enzyme';
import Component from 'components/Head';

const props = {
  title: 'Head Component Title',
  description: 'Head Description',
  canonical: 'Head Canonical URL',
  favicon: '/images/head.svg',
  siteName: 'Head Site Name'
};

describe('COMPONENT -> Head -> Mock properties...', () => {
  let component = shallow(
    <Component { ...props } />
  );

  it(`expect canonical link href to equal "${props.canonical}"...`, () => {
    expect(component.find('link[rel="canonical"]').props().href).toBe(props.canonical);
  });

  it(`expect favicon href to equal "${props.favicon}"...`, () => {
    expect(component.find('link[rel="icon"]').props().href).toBe(props.favicon);
  });
});

describe('COMPONENT -> Head -> snapshot...', () => {
  let component = shallow(<Component { ...props } />);

  it('expect to render correct properties', () => {
    expect(component).toMatchSnapshot();
  });
});

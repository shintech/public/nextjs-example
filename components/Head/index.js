import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';

const HeadComponent = ({ canonical, favicon }) =>
  <Head>
    <meta name='viewport' content='initial-scale=1.0, width=device-width' />
    <link rel='canonical' href={canonical} />
    <link id='favicon' rel='icon' href={favicon} />
  </Head>;

HeadComponent.propTypes = {
  canonical: PropTypes.string,
  favicon: PropTypes.string
};

HeadComponent.defaultProps = {
  canonical: '',
  favicon: '/images/icon.svg'
};

export default HeadComponent;

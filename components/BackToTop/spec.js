/* eslint-env jest */

import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import BackToTop from 'components/BackToTop';

const props = {};

describe('COMPONENT -> BackToTop -> Mock properties...', () => {
  let shallow;

  beforeAll(() => {
    shallow = createShallow();
  });

  // it(`expect h1 text to equal "${props.phone}"...`, () => {
  //   let component = shallow(<Footer { ...props }/>);

  //   expect(component.find('.header').text()).toBe(props.phone);
  // });

  // it(`expect p text to equal "${props.email}"...`, () => {
  //   let component = shallow(<Footer { ...props }/>);

  //   expect(component.find('.body').text()).toBe(props.email);
  // });

  it('expect snapshot to pass...', () => {
    let component = shallow(<BackToTop {...props}/>);

    expect(component).toMatchSnapshot();
  });
});

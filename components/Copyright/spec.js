/* eslint-env jest */

import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import Copyright from 'components/Copyright';

const props = {
  siteName: 'siteName'
};

describe('COMPONENT -> Copyright -> Mock properties...', () => {
  let shallow;

  beforeAll(() => {
    shallow = createShallow();
  });

  // it(`expect h1 text to equal "${props.title}"...`, () => {
  //   let component = shallow(<Card { ...props }/>);

  //   expect(component.find('.header').text()).toBe(props.title);
  // });

  // it(`expect p text to equal "${props.email}"...`, () => {
  //   let component = shallow(<Footer { ...props }/>);

  //   expect(component.find('.body').text()).toBe(props.email);
  // });

  it('expect snapshot to pass...', () => {
    let component = shallow(<Copyright { ...props }/>);

    expect(component).toMatchSnapshot();
  });
});

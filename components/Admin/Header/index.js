import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Hidden from '@material-ui/core/Hidden';
import Fab from '@material-ui/core/Fab';

import Home from '@material-ui/icons/Home';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';

import Link from 'components/Link';
import SideDrawer from 'components/SideDrawer';
import HideOnScroll from 'components/HideOnScroll';
import BackToTop from 'components/BackToTop';

const useStyles = makeStyles((theme) => ({
  navContainer: {
    display: `flex`,
    justifyContent: `space-between`
  },

  navList: {
    display: `flex`
  },

  link: {
    color: `white`,
    textDecoration: `none`,
    textTransform: `uppercase`,

    '&.is-active': {
      color: theme.palette.primary.contrastText,
      cursor: 'not-allowed',

      '& > *': {
        cursor: 'not-allowed'
      }
    }
  }
}));

const navLinks = [
  { title: `admin`, path: `/admin` },
  { title: `login`, path: `/admin/login` }
  // { title: `blog`, path: `/blog` }
];

const Header = () => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <HideOnScroll>
        <AppBar position="fixed">
          <Toolbar>
            <Container className={classes.navContainer} maxWidth="xl">
              <IconButton component={Link} naked={true} href='/' edge="start" color="inherit" aria-label="home">
                <Home color='disabled' fontSize="large" />
              </IconButton>

              <Hidden smDown>
                <List component="nav" aria-labelledby="main navigation" className={classes.navList}>
                  {navLinks.map(({ title, path }) => (
                    <ListItem component={Link} href={path} className={classes.link} activeClassName='is-active' key={title} button>
                      <ListItemText primary={title} />
                    </ListItem>
                  ))}
                </List>
              </Hidden>
              <Hidden mdUp>
                <SideDrawer navLinks={navLinks}/>
              </Hidden>
            </Container>
          </Toolbar>
        </AppBar>
      </HideOnScroll>

      <Toolbar id="back-to-top-anchor" />

      <BackToTop>
        <Fab color="primary" size="large" aria-label="scroll back to top">
          <KeyboardArrowUp />
        </Fab>
      </BackToTop>
    </React.Fragment>
  );
};

Header.propTypes = {
  siteName: PropTypes.string,
  title: PropTypes.string,
  theme: PropTypes.object
};

Header.defaultProps = {
  siteName: 'Default Footer Site',
  title: 'Default Footer'
};

export default Header;

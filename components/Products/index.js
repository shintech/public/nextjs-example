import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Card from 'components/Card';

const Products = ({ products }) =>
  <Grid
    container
    direction="row"
    justify="center"
    alignItems="flex-end"
    spacing={4}
  >
    {products.map(product =>
      <Grid item key={product.title} md={3} sm={6} xs={12}>
        <Card { ...product }/>
      </Grid>
    )}
  </Grid>;

Products.propTypes = {
  products: PropTypes.array
};

export default Products;

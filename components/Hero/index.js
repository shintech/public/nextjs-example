import React from 'react';
import Image from 'next/image';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ReactMarkdown from 'react-markdown';

import Link from 'components/Link';

const useStyles = makeStyles((theme) => ({
  buttons: {
    display: 'flex',

    '& > a': {
      minWidth: theme.spacing(20),

      '&:first-child': {
        marginRight: theme.spacing(1)
      }
    }
  }
}));

const Hero = ({ title = 'title', content = 'content', image = 'default.svg', links }) => {
  const classes = useStyles();

  return (
    <Grid
      container
      alignItems='flex-end'
      spacing={2}
    >
      <Grid md={8} xs={12} item>
        <Box>
          <Typography className='header' component="h1" variant="h2" color="textPrimary" gutterBottom>{ title }</Typography>

          <Typography className='body' variant="body1" gutterBottom>
            <ReactMarkdown>{content}</ReactMarkdown>
          </Typography>
        </Box>

        {(links && links.length > 0) &&
          <Box className={classes.buttons}>
            {
              links.map(link =>
                <Button
                  key={link.name}
                  component={Link}
                  naked={true}
                  href={link.href}
                  variant="contained"
                  color={link.color || 'default'}
                >
                  {link.message}
                </Button>)
            }
          </Box>
        }
      </Grid>

      <Grid md={4} xs={12} item>
        <Image loading='eager' priority={true} layout='responsive' width='100%' height='80%' src={`/images/${image}`}/>
      </Grid>
    </Grid>
  );
};

Hero.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string,
  image: PropTypes.string,
  links: PropTypes.array
};

Hero.defaultProps = {
  title: 'Default',
  content: 'Default',
  image: 'icon.svg',
  links: []
};

export default Hero;

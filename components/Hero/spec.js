/* eslint-env jest */

import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import Hero from 'components/Hero';
import SVG from 'public/images/icon.svg';

const props = {
  title: 'Hero Title',
  content: 'Hero Content',
  SVG
};

describe('COMPONENT -> Hero -> Mock properties...', () => {
  let shallow;

  beforeAll(() => {
    shallow = createShallow();
  });

  // it(`expect h1 text to equal "${props.title}"...`, () => {
  //   let component = shallow(<Hero { ...props }/>);

  //   expect(component.find('.header').text()).toBe(props.title);
  // });

  // it(`expect p text to equal "${props.content}"...`, () => {
  //   let component = shallow(<Hero { ...props }/>);

  //   expect(component.find('.body').text()).toBe(props.content);
  // });

  it('expect snapshot to pass...', () => {
    let component = shallow(<Hero { ...props }/>);

    expect(component).toMatchSnapshot();
  });
});

import { useState, useRef, useEffect } from 'react';
import { createPortal } from 'react-dom';

const RenderInWindow = (props) => {
  const [container, setContainer] = useState(null);
  const newWindow = useRef(null);

  useEffect(() => {
    setContainer(document.createElement('div'));
  }, []);

  useEffect(() => {
    if (container) {
      const windowSize = {
        width: 1200,
        height: 850
      };

      const windowLocation = {
        left: (window.screen.availLeft + (window.screen.availWidth / 2)) - (windowSize.width / 2),
        top: (window.screen.availTop + (window.screen.availHeight / 2)) - (windowSize.height / 2)
      };

      newWindow.current = window.open(
        '',
        '',
        'width=' + windowSize.width + ', height=' + windowSize.height + ', left=' + windowLocation.left + ', top=' + windowLocation.top
      );

      newWindow.current.document.body.appendChild(container);

      const stylesheets = Array.from(document.styleSheets);
      stylesheets.forEach(stylesheet => {
        let css = stylesheet;

        if (stylesheet.href) {
          const newStyleElement = document.createElement('link');
          newStyleElement.rel = 'stylesheet';
          newStyleElement.href = stylesheet.href;
          newWindow.current.document.head.appendChild(newStyleElement);
        } else if (css && css.cssRules && css.cssRules.length > 0) {
          const newStyleElement = document.createElement('style');
          Array.from(css.cssRules).forEach(rule => {
            newStyleElement.appendChild(document.createTextNode(rule.cssText));
          });
          newWindow.current.document.head.appendChild(newStyleElement);
        }
      });

      newWindow.current.document.title = props.title;

      newWindow.current.addEventListener('beforeunload', () => {
        props.closeWindow();
      });

      const curWindow = newWindow.current;

      return () => curWindow.close();
    }
  }, [container]);

  return container && createPortal(props.children, container);
};

export default RenderInWindow;

/* eslint-env jest */

import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import Typography from '@material-ui/core/Typography';
import Card from 'components/Card';

const props = {
  slug: '/products/title',
  title: 'title',
  description: 'description'
};

describe('COMPONENT -> Card -> Mock properties...', () => {
  let shallow;

  beforeAll(() => {
    shallow = createShallow();
  });

  it(`expect h1 text to equal "${props.title}"...`, () => {
    let component = shallow(<Card { ...props }/>);
    expect(component.find(Typography).first().text()).toBe(props.title);
  });

  it(`expect p text to equal "${props.description}"...`, () => {
    let component = shallow(<Card { ...props }/>);
    expect(component.find(Typography).last().text()).toBe(props.description);
  });

  it('expect snapshot to pass...', () => {
    let component = shallow(<Card { ...props }/>);

    expect(component).toMatchSnapshot();
  });
});

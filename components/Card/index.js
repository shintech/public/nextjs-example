import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Link from 'components/Link';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  }
});

const SimpleCard = function ({ slug, title, description }) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography variant="h5" component="h2">
          {title}
        </Typography>
        <Typography variant="body2" component="p">
          {description}
        </Typography>
      </CardContent>

      <CardActions>
        <Button variant='text' component={Link} naked={true} href={`/products/${slug}`} size="large">Learn More</Button>
      </CardActions>
    </Card>
  );
};

SimpleCard.propTypes = {
  title: PropTypes.string,
  slug: PropTypes.string,
  description: PropTypes.string
};

export default SimpleCard;

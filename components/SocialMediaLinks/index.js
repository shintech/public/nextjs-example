import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import IconButton from '@material-ui/core/IconButton';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';

const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.text,
    marginTop: theme.spacing(5)
  }
}));

export default function SocialMediaLinks ({ facebook, twitter, linkedin }) {
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      <IconButton size='small' component='a' href={facebook} color="inherit" aria-label="Facebook">
        <FacebookIcon/>
      </IconButton>

      <IconButton size='small' component='a' href={twitter} color="inherit" aria-label="Twitter">
        <TwitterIcon/>
      </IconButton>

      <IconButton size='small' component='a' href={linkedin} color="inherit" aria-label="LinkedIn">
        <LinkedInIcon/>
      </IconButton>
    </Container>
  );
}

SocialMediaLinks.propTypes = {
  facebook: PropTypes.string,
  twitter: PropTypes.string,
  linkedin: PropTypes.string
};

SocialMediaLinks.defaultProps = {
  facebook: 'https://www.facebook.com/',
  twitter: 'https://twitter.com/',
  linkedin: 'https://www.linkedin.com/'
};

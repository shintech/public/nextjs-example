import React from 'react';
import PropTypes from 'prop-types';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import BusinessIcon from '@material-ui/icons/Business';
import { makeStyles } from '@material-ui/core/styles';

import Form from 'components/Form';
import SocialMediaLinks from 'components/SocialMediaLinks';
import Copyright from 'components/Copyright';

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',
    background: theme.palette.secondary.dark,
    marginTop: theme.spacing(4),
    paddingTop: theme.spacing(1)
  },

  sub: {
    paddingBottom: theme.spacing(4)
  },

  list: {
    margin: theme.spacing(1, 'auto'),
    '& a': {
      margin: theme.spacing(1, 'auto')
    }
  }
}));

const Footer = (props) => {
  const classes = useStyles();
  const regEx = /^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})/;
  const replacementPattern = '($1) $2-$3';

  const map = new Map(props.properties);

  return (
    <Container className={classes.root} maxWidth='xl'>
      <Typography variant="h4" component="h4">Contact Us!</Typography>

      <Container className={classes.sub} maxWidth='md'>
        <List className={classes.list}>
          <ListItem button component="a" href={`tel:${map.get('phone')}`}>
            <ListItemAvatar>
              <PhoneIcon/>
            </ListItemAvatar>

            <ListItemText primary={map.get('phone').replace(regEx, replacementPattern)}/>
          </ListItem>

          <ListItem button component="a" href={`mailto:${map.get('email')}`}>
            <ListItemAvatar>
              <EmailIcon/>
            </ListItemAvatar>

            <ListItemText primary={map.get('email')}/>
          </ListItem>

          <ListItem component="li">
            <ListItemAvatar>
              <BusinessIcon/>
            </ListItemAvatar>

            <ListItemText primary={map.get('address')}/>
          </ListItem>
        </List>

        <Form/>
      </Container>

      <SocialMediaLinks/>
      <Copyright siteName={map.get('sitename')}/>
    </Container>
  );
};

Footer.propTypes = {
  properties: PropTypes.array
};

export default Footer;

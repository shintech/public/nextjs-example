/* eslint-env jest */

import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';

import Footer from 'components/Footer';
import Form from 'components/Form';
import SocialMediaLinks from 'components/SocialMediaLinks';
import Copyright from 'components/Copyright';

const properties = [
  ['phone', '5555555555'],
  ['address', '123 St Address, City, ST, 12345'],
  ['email', 'test@example.org'],
  ['sitename', 'Tulsa Tech Services']
];

const props = {
  title: 'Contact Us',
  phone: '1234567890',
  email: 'test@example.org',
  properties
};

describe('COMPONENT -> Footer -> Mock properties...', () => {
  let shallow;

  beforeAll(() => {
    shallow = createShallow();
  });

  it(`expect h1 text to equal "Contact Us!"...`, () => {
    let component = shallow(<Footer { ...props }/>);
    expect(component.find(Typography).text()).toBe('Contact Us!');
  });

  it(`expect each element of list to render...`, () => {
    let component = shallow(<Footer { ...props }/>);

    component.find(List).children().forEach(node => {
      expect(node.exists()).toEqual(true);
    });
  });

  it(`expect Form component to render...`, () => {
    let component = shallow(<Footer { ...props }/>);
    expect(component.find(Form).exists()).toEqual(true);
  });

  it(`expect SocialMediaLinks component to render...`, () => {
    let component = shallow(<Footer { ...props }/>);
    expect(component.find(SocialMediaLinks).exists()).toEqual(true);
  });

  it(`expect Copyright component to render...`, () => {
    let component = shallow(<Footer { ...props }/>);
    expect(component.find(Copyright).exists()).toEqual(true);
  });

  it('expect snapshot to pass...', () => {
    let component = shallow(<Footer { ...props }/>);

    expect(component).toMatchSnapshot();
  });
});

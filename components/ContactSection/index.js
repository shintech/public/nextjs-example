import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import BusinessIcon from '@material-ui/icons/Business';

import Form from 'components/Form';
import SocialMediaLinks from 'components/SocialMediaLinks';
import Copyright from 'components/Copyright';

const regEx = /^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})/;
const replacementPattern = '($1) $2-$3';

const useStyles = makeStyles((theme) => ({
  sub: {
    textAlign: 'center'
  },
  bottom: {
    marginTop: theme.spacing(20)
  }
}));

const ContactInfo = ({ info }) => {
  const classes = useStyles();

  const map = new Map(info);

  return (
    <Container className={classes.sub} >
      <Grid
        container
        direction="row"
        alignItems="flex-start"
        spacing={2}
      >
        <Grid item sm={12} md={4}>
          <List>
            <ListItem button component="a" href={`tel:${map.get('phone')}`}>
              <ListItemAvatar>
                <PhoneIcon/>
              </ListItemAvatar>

              <ListItemText primary={map.get('phone').replace(regEx, replacementPattern)}/>
            </ListItem>

            <ListItem button component="a" href={`mailto:${map.get('email')}`}>
              <ListItemAvatar>
                <EmailIcon/>
              </ListItemAvatar>

              <ListItemText primary={map.get('email')}/>
            </ListItem>

            <ListItem component="li">
              <ListItemAvatar>
                <BusinessIcon/>
              </ListItemAvatar>

              <ListItemText primary={map.get('address')}/>
            </ListItem>
          </List>
        </Grid>

        <Grid item sm={12} md={8}>
          <Form/>
        </Grid>
      </Grid>

      <Container className={classes.bottom}>
        <SocialMediaLinks/>
        <Copyright siteName={map.get('sitename')}/>
      </Container>
    </Container>
  );
};

ContactInfo.propTypes = {
  info: PropTypes.array.isRequired
};

export default ContactInfo;

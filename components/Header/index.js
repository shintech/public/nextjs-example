import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Hidden from '@material-ui/core/Hidden';
import Fab from '@material-ui/core/Fab';

import Home from '@material-ui/icons/Home';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';

import Link from 'components/Link';
import SideDrawer from 'components/SideDrawer';
import HideOnScroll from 'components/HideOnScroll';
import BackToTop from 'components/BackToTop';

const useStyles = makeStyles((theme) => ({
  navContainer: {
    display: `flex`,
    justifyContent: `space-between`
  },

  navList: {
    display: `flex`
  },

  link: {
    color: `white`,
    textDecoration: `none`,
    textTransform: `uppercase`,

    '&.is-active': {
      color: theme.palette.primary.contrastText,
      cursor: 'not-allowed',

      '& > *': {
        cursor: 'not-allowed'
      }
    }
  }
}));

const Header = ({ routes, position, disable }) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <HideOnScroll>
        <AppBar position={position}>
          <Toolbar>
            <Container className={classes.navContainer} maxWidth="xl">
              <IconButton
                component={Link}
                naked={true}
                href='/'
                edge="start"
                color="inherit"
                aria-label="home"
                disabled={disable}
              >
                <Home color='disabled' fontSize="large" />
              </IconButton>

              <Hidden smDown>
                <List component="nav" aria-labelledby="main navigation" className={classes.navList}>
                  {routes.map(({ title, path }) => (
                    <ListItem component={Link} href={path} className={classes.link} activeClassName='is-active' key={title} disabled={disable} button>
                      <ListItemText primary={title} />
                    </ListItem>
                  ))}
                </List>
              </Hidden>
              <Hidden mdUp>
                <SideDrawer navLinks={routes}/>
              </Hidden>
            </Container>
          </Toolbar>
        </AppBar>
      </HideOnScroll>

      <Toolbar id="back-to-top-anchor" />

      <BackToTop>
        <Fab color="primary" size="large" aria-label="scroll back to top">
          <KeyboardArrowUp />
        </Fab>
      </BackToTop>
    </React.Fragment>
  );
};

Header.propTypes = {
  routes: PropTypes.array.isRequired,
  position: PropTypes.string,
  disable: PropTypes.bool
};

Header.defaultProps = {
  postion: 'sticky',
  disable: false,
  routes: [
    { title: `home`, path: `/` }
  ]
};

export default Header;

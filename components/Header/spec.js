/* eslint-env jest */

import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import AppBar from '@material-ui/core/AppBar';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';

import Header from 'components/Header';

const props = {};

const navLinks = [
  { title: `home`, path: `/` },
  { title: `about`, path: `/about` },
  { title: `contact`, path: `/contact` }
  // { title: `blog`, path: `/blog` }
];

describe('COMPONENT -> Header -> Mock properties...', () => {
  let shallow;

  beforeAll(() => {
    shallow = createShallow();
  });

  it(`expect AppBar to render...`, () => {
    let component = shallow(<Header { ...props }/>);
    expect(component.find(AppBar).exists()).toEqual(true);
  });

  it(`expect each nav-link to render...`, () => {
    let component = shallow(<Header { ...props }/>);

    component.find(List).children().forEach((node, v) => {
      expect(node.exists()).toEqual(true);
      expect(node.find(ListItem).props().href).toEqual(navLinks[v].path);
      expect(node.find(ListItemText).props().primary).toEqual(navLinks[v].title);
    });
  });

  it('expect snapshot to pass...', () => {
    let component = shallow(<Header { ...props }/>);

    expect(component).toMatchSnapshot();
  });
});

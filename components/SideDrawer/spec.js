/* eslint-env jest */

import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import SideDrawer from 'components/SideDrawer';

const props = {
  navLinks: [
    { title: `home`, path: `/` },
    { title: `about`, path: `/about` },
    { title: `contact`, path: `/contact` }
    // { title: `blog`, path: `/blog` }
  ]
};

describe('COMPONENT -> SideDrawer -> Mock properties...', () => {
  let shallow;

  beforeAll(() => {
    shallow = createShallow();
  });

  // it(`expect h1 text to equal "${props.title}"...`, () => {
  //   let component = shallow(<SideDrawer { ...props }/>);

  //   expect(component.find('.header').text()).toBe(props.title);
  // });

  // it(`expect p text to equal "${props.content}"...`, () => {
  //   let component = shallow(<SideDrawer { ...props }/>);

  //   expect(component.find('.body').text()).toBe(props.content);
  // });

  it('expect snapshot to pass...', () => {
    let component = shallow(<SideDrawer { ...props }/>);

    expect(component).toMatchSnapshot();
  });
});

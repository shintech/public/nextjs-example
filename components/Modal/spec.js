/* eslint-env jest */

import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import Modal from 'components/Modal';

const props = {
  open: true,
  title: 'title',
  message: 'message',
  onClose: () => {}
};

describe('COMPONENT -> Modal -> Mock properties...', () => {
  let shallow;

  beforeAll(() => {
    shallow = createShallow();
  });

  // it(`expect h1 text to equal "${props.title}"...`, () => {
  //   let component = shallow(<Modal { ...props }/>);

  //   expect(component.find('.header').text()).toBe(props.title);
  // });

  // it(`expect p text to equal "${props.content}"...`, () => {
  //   let component = shallow(<Modal { ...props }/>);

  //   expect(component.find('.body').text()).toBe(props.content);
  // });

  it('expect snapshot to pass...', () => {
    let component = shallow(<Modal { ...props }/>);

    expect(component).toMatchSnapshot();
  });
});

/* eslint-env jest */

import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import Loader from 'components/Loader';

const props = {};

describe('COMPONENT -> Loader -> Mock properties...', () => {
  let shallow;

  beforeAll(() => {
    shallow = createShallow();
  });

  it('expect snapshot to pass...', () => {
    let component = shallow(<Loader { ...props }/>);

    expect(component).toMatchSnapshot();
  });
});

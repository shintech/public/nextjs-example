import React, { useState, createRef } from 'react';
import PropTypes from 'prop-types';
import { gql, useMutation } from '@apollo/client';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MuiAlert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import ReCAPTCHA from 'react-google-recaptcha';
import green from '@material-ui/core/colors/green';

import validate from 'lib/validation';

const recaptchaSiteKey = (process.env['NODE_ENV'] !== 'production') ? '6LesxbYUAAAAANA_Si5ylzSGwgbX8O7uECnPZ_p8' : '6LfvffgZAAAAAOW13Qs7X6-dlXZASi4wokSYRWye';

const recaptchaRef = createRef();

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 0,
    alignItems: 'center'
  },

  form: {
    '& .wrapper': {
      margin: theme.spacing(1),
      position: 'relative'
    },

    '& .MuiFormHelperText-root.Mui-error::first-letter': {
      textTransform: 'capitalize'
    }
  },

  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  }
}));

const initialState = {
  name: '',
  email: '',
  message: ''
};

const MUTATION = gql`
  mutation emailMutation($email: String!, $message: String!, $name: String!) {
    createEmail(email: $email, message: $message, name: $name) {
      message
    }
  }
`;

const Form = () => {
  const [values, setValues] = useState(initialState);
  const [errors, setErrors] = useState({});
  const [touched, setTouched] = useState({});
  const [open, setOpen] = useState(false);

  const classes = useStyles();

  const [createEmail, {
    loading: mutationLoading,
    error: mutationError
  }] = useMutation(MUTATION);

  const handleChange = e => {
    const { name, value: newValue, type } = e.target;

    // keep number fields as numbers
    const value = type === 'number' ? +newValue : newValue;

    // save field values
    setValues({
      ...values,
      [name]: value
    });

    // was the field modified
    setTouched({
      ...touched,
      [name]: true
    });
  };

  const handleBlur = e => {
    const { name, value } = e.target;

    // remove whatever error was there previously
    const { [name]: removedError, ...rest } = errors;

    // check for a new error
    const error = validate(name, value);

    // // validate the field if the value has been touched
    setErrors({
      ...rest,
      ...(error && { [name]: touched[name] && error })
    });
  };

  const handleSubmit = async e => {
    e.preventDefault();
    // validate the form
    const formValidation = Object.keys(values).reduce(
      (acc, key) => {
        const newError = validate(key, values[key]);
        const newTouched = { [key]: true };

        return {
          errors: {
            ...acc.errors,
            ...(newError && { [key]: newError })
          },
          touched: {
            ...acc.touched,
            ...newTouched
          }
        };
      },
      {
        errors: { ...errors },
        touched: { ...touched }
      }
    );

    setErrors(formValidation.errors);
    setTouched(formValidation.touched);

    if (
      !Object.values(formValidation.errors).length && // errors object is empty
      Object.values(formValidation.touched).length === Object.values(values).length && // all fields were touched
      Object.values(formValidation.touched).every(t => t === true) // every touched field is true
    ) {
      try {
        const token = await recaptchaRef.current.executeAsync();

        await createEmail({
          variables: {
            message: values.message,
            email: values.email,
            name: values.name
          },

          context: {
            headers: {
              'g-recaptcha-response': token
            }
          }
        });

        setOpen(true);
        setValues(initialState);
        recaptchaRef.current.reset();
      } catch (err) {
        console.error(err.message); // eslint-disable-line
      }
    }
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  };

  return (
    <Container className={classes.root}>
      <form className={classes.form} onSubmit={handleSubmit}>
        <TextField
          fullWidth
          variant="filled"
          className={classes.textField}
          label="Name"
          name="name"
          value={values.name}
          onChange={handleChange}
          onBlur={handleBlur}
          error={!!errors.name}
          helperText={(errors.name) ? errors.name : ' '}
        />

        <TextField
          fullWidth
          variant="filled"
          className={classes.textField}
          label="Email"
          name='email'
          value={values.email}
          onChange={handleChange}
          onBlur={handleBlur}
          error={!!errors.email}
          helperText={(errors.email) ? errors.email : ' '}
        />

        <TextField
          label="Message"
          name='message'
          multiline
          fullWidth
          rows={6}
          variant="filled"
          className={classes.textField}
          value={values.message}
          onChange={handleChange}
          onBlur={handleBlur}
          error={!!errors.message}
          helperText={(errors.message) ? errors.message : ' '}
        />

        <ReCAPTCHA
          theme='dark'
          sitekey={recaptchaSiteKey}
          ref={recaptchaRef}
          size="invisible"
        />

        <div className='wrapper'>
          <Button type='submit' variant='contained' size='large' disabled={mutationLoading} >
            Submit
          </Button>

          {mutationLoading && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>
      </form>

      <Snackbar open={open} autoHideDuration={15000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={(mutationError) ? 'error' : 'success'}>
          {mutationError || 'Success...'}
        </Alert>
      </Snackbar>
    </Container>
  );
};

Form.propTypes = {
  children: PropTypes.node,
  theme: PropTypes.object
};

Form.defaultProps = {
  theme: {}
};

export default Form;

/* eslint-env jest */

import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import Form from 'components/Form';

const props = {};

describe('COMPONENT -> Form -> Mock properties...', () => {
  let shallow;

  beforeAll(() => {
    shallow = createShallow();
  });

  // it(`expect h1 text to equal "${props.title}"...`, () => {
  //   let component = shallow(<Card { ...props }/>);

  //   expect(component.find('.header').text()).toBe(props.title);
  // });

  // it(`expect p text to equal "${props.email}"...`, () => {
  //   let component = shallow(<Footer { ...props }/>);

  //   expect(component.find('.body').text()).toBe(props.email);
  // });

  it('expect snapshot to pass...', () => {
    let component = shallow(<Form { ...props }/>);

    expect(component).toMatchSnapshot();
  });
});

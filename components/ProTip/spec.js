/* eslint-env jest */

import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import ProTip from 'components/ProTip';

const props = {
  href: '/'
};

describe('COMPONENT -> ProTip -> Mock properties...', () => {
  let shallow;

  beforeAll(() => {
    shallow = createShallow();
  });

  // it(`expect h1 text to equal "${props.title}"...`, () => {
  //   let component = shallow(<ProTip { ...props }/>);

  //   expect(component.find('.header').text()).toBe(props.title);
  // });

  // it(`expect p text to equal "${props.content}"...`, () => {
  //   let component = shallow(<ProTip { ...props }/>);

  //   expect(component.find('.body').text()).toBe(props.content);
  // });

  it('expect snapshot to pass...', () => {
    let component = shallow(<ProTip { ...props }/>);

    expect(component).toMatchSnapshot();
  });
});

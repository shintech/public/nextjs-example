import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import { useReactiveVar } from '@apollo/client';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import Header from 'components/Header';
import Hero from 'components/Hero';
import Products from 'components/Products';
import ContactSection from 'components/ContactSection';
import Footer from 'components/Footer';
import PageTransition from 'components/PageTransition';

import { snackbarVar } from 'lib/client/vars';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2)
  }
}));

const Template = ({ page, routes, baseURL, footer, header, hero, contactForm, preview, children }) => {
  const classes = useStyles();
  const snackbar = useReactiveVar(snackbarVar);

  const { title, content, image, properties, sections, links, products } = page;

  const { host } = new URL(baseURL);

  const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    snackbarVar({
      open: false,
      message: '',
      severity: 'info'
    });
  };

  return (
    <>
  <Head>
    <title>{ title } | { host }</title>
    <meta name='title' content={`${title} | ${host}`} />
    <meta name='description' content={ content } />
  </Head>

    {header && <Header routes={routes}/>}

    <PageTransition>
      <Container className={classes.root}>
        {hero && <Hero image={image} title={title} content={content} links={links}/>}

        {products && <Products products={products}/>}

        {
          sections &&
            sections.map(section => <Hero key={section.title} image={section.image} title={section.title} content={section.content}/>)
        }

        {contactForm && <ContactSection info={properties}/>}

        {children}
      </Container>
    </PageTransition>

    {footer && <Footer properties={properties}/>}

    {!preview && <Snackbar open={snackbar.open} autoHideDuration={10000} onClose={handleClose}>
      <Alert onClose={handleClose} severity={snackbar.severity}>
        {snackbar.message}
      </Alert>
    </Snackbar>}
  </>
  );
};

Template.propTypes = {
  page: PropTypes.object,
  routes: PropTypes.array,
  baseURL: PropTypes.string,
  footer: PropTypes.bool,
  header: PropTypes.bool,
  hero: PropTypes.bool,
  contactForm: PropTypes.bool,
  title: PropTypes.string,
  preview: PropTypes.bool,
  children: PropTypes.node
};

Template.defaultProps = {
  baseURL: 'www.example.org',
  footer: true,
  header: true,
  hero: true,
  preview: false,
  contactForm: false
};

export default Template;

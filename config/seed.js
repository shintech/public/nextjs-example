const { MongoClient } = require('mongodb');

// props is an array of objects containing the collection name and
// an array of the data to be inserted
//
// const props = [
//   {
//     collection: 'collectionName',
//     body: [
//       username: 'username',
//       password: 'password',
//       ...
//     ]
//   },
//   ...
// ]

module.exports = async (props, message = 'Finished', drop = false) => {
  try {
    const host = process.env['MONGO_HOST'] || 'localhost';
    const port = process.env['MONGO_PORT'] || 27017;
    const user = process.env['MONGO_INITDB_ROOT_USERNAME'];
    const pass = process.env['MONGO_INITDB_ROOT_PASSWORD'];

    const dbClient = new MongoClient(
      `mongodb://${user}:${pass}@${host}:${port}?authSource=admin`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    );

    if (!dbClient.isConnected()) await dbClient.connect();

    const db = dbClient.db(process.env['MONGO_INITDB_DATABASE']);

    const insert = (collection, body) => new Promise((resolve, reject) => {
      db.collection(collection).insertMany(body, (err, { result: { n } }) => {
        if (err) return reject(err);

        resolve(
          console.log(`[${collection}] - successfully inserted "${n}" ${n > 1 ? `records` : 'record'}...`) // eslint-disable-line
        );
      });
    });

    if (drop) {
      // eslint-disable-next-line no-console
      console.warn(`[${db.databaseName}] - drop = "${drop}"; dropping database...`);
      await db.dropDatabase();
    }

    await Promise.all(props.map(prop =>
      insert(prop.collection, prop.body)
    ));

    console.log(`${message}`); // eslint-disable-line

    dbClient.close();
  } catch (err) {
    console.error('--->error while connecting to db'); // eslint-disable-line
    console.error(err); // eslint-disable-line
  }
};

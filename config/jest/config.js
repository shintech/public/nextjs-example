const path = require('path');

module.exports = {
  testPathIgnorePatterns: ['<rootDir>/node_modules/'],
  rootDir: path.join('..', '..'),
  setupFiles: ['<rootDir>/config/jest/setup.js'],
  testEnvironment: 'jsdom',
  snapshotSerializers: ['enzyme-to-json/serializer'],
  testRegex: 'spec\\.js$',
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga|ico|css|less|gql|graphql)$': '<rootDir>/config/jest/fileMock.js'
  }
};

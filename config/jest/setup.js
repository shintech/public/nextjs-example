const jest = require('jest');
const { configure } = require('enzyme');
const Adapter = require('enzyme-adapter-react-16');

const NODE_ENV = process.env['NODE_ENV'] || 'test';
const PORT = process.env['PORT'] || 8001;
const HOST = process.env['HOST'] || 'localhost';

jest.mock('next/config', () => () => ({
  publicRuntimeConfig: {
    NODE_ENV: 'test',
    VALUE: 'value',
    siteName: 'siteName',
    theme: {}
  }
}));

global.process.env = {
  NODE_ENV,
  PORT,
  HOST
};

configure({ adapter: new Adapter() });

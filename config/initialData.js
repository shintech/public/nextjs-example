const faker = require('faker');
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const slugify = require('slugify');
const seed = require('./seed');

const generatePassword = (
  length = 20,
  wishlist = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#$'
) =>
  Array.from(crypto.randomFillSync(new Uint32Array(length)))
    .map((x) => wishlist[x % wishlist.length])
    .join('');

const username = process.argv[2] || 'admin';
const password = (process.env['NODE_ENV'] === 'production') ? generatePassword(30) : 'password';

const users = {
  collection: 'users',
  body: [
    {
      username,
      password: bcrypt.hashSync(password, 10)
    }
  ]
};

const products = new Array(4).fill(null).map((f, v) => {
  const title = faker.commerce.productName();
  const slug = slugify(title, { lower: true });

  const images = ['analytics.svg', 'building.svg', 'world.svg', 'developer.svg'];

  return {
    title,
    slug,
    description: faker.lorem.sentences(),
    content: faker.lorem.paragraphs(),
    image: images[v]
  };
});

const properties = [
  ['phone', '5555555555'],
  ['address', '123 St Address, City, ST, 12345'],
  ['email', 'test@example.org'],
  ['sitename', 'Tulsa Tech Services']
];

const pages = {
  collection: 'pages',
  body: [
    {
      title: 'Tulsa Tech Services',
      description: faker.hacker.phrase(),
      content: `---\n${faker.lorem.paragraphs()}`,
      slug: 'home',
      image: 'moon.svg',
      sections: [],
      properties,
      products,
      links: [
        {
          name: 'about',
          message: 'About Us',
          href: '/about'
        },

        {
          name: 'contact',
          message: 'Schedule a Consultation',
          href: '/contact',
          color: 'primary'
        }
      ]
    },

    {
      title: 'About Us',
      description: faker.hacker.phrase(),
      content: `---\n${faker.lorem.paragraphs()}`,
      slug: 'about',
      image: 'hologram.svg',
      properties,
      sections: [
        {
          title: faker.hacker.noun(),
          description: faker.hacker.phrase(),
          content: faker.lorem.paragraphs(),
          image: 'analytics.svg'
        },

        {
          title: faker.hacker.noun(),
          description: faker.hacker.phrase(),
          content: faker.lorem.paragraphs(),
          image: 'progressive.svg'
        },

        {
          title: faker.hacker.noun(),
          description: faker.hacker.phrase(),
          content: faker.lorem.paragraphs(),
          image: 'worktime.svg'
        }
      ]
    },

    {
      title: 'Contact Us',
      description: faker.hacker.phrase(),
      content: `---\n${faker.lorem.paragraphs()}`,
      slug: 'contact',
      image: 'contact.svg',
      sections: [],
      properties
    },

    {
      title: 'Product',
      description: faker.hacker.phrase(),
      content: `---\n${faker.lorem.paragraphs()}`,
      slug: 'product',
      image: '',
      sections: [],
      properties
    },

    {
      title: 'Not Found',
      description: faker.hacker.phrase(),
      content: `---\n${faker.lorem.paragraphs()}`,
      slug: 'notfound',
      image: '',
      sections: [],
      properties
    },

    {
      title: 'Admin',
      description: faker.hacker.phrase(),
      content: `---\n${faker.lorem.paragraphs()}`,
      slug: 'admin',
      image: '',
      sections: [],
      properties
    },

    {
      title: 'Blog',
      description: faker.hacker.phrase(),
      content: `---\n${faker.lorem.paragraphs()}`,
      slug: 'blog',
      image: '/images/icon.svg'
    }
  ]
};

const message = `Finished - login credentials: ${username}:${password}`;

seed([users, pages], message, true);

// const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const seed = require('./seed');

const generatePassword = (
  length = 20,
  wishlist = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#$'
) =>
  Array.from(crypto.randomFillSync(new Uint32Array(length)))
    .map((x) => wishlist[x % wishlist.length])
    .join('');

const username = process.argv[2] || 'admin';
const password = (process.env['NODE_ENV'] === 'production') ? generatePassword(30) : 'password';

const users = {
  collection: 'users',
  body: [
    {
      username,
      password: bcrypt.hashSync(password, 10)
    }
  ]
};

const message = `Finished - login credentials: ${username}:${password}`;

seed([users], message, false);

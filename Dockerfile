FROM node:14.7.0

RUN useradd -m -d /home/web web && \
  mkdir -p /home/web/app/node_modules /home/web/app/log && \
  chown -R web:web /home/web

WORKDIR /home/web/app

RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 && \
  chmod +x /usr/local/bin/dumb-init

ARG NEXT_PUBLIC_URI
ENV NEXT_PUBLIC_URI=$NEXT_PUBLIC_URI

COPY package.json .
COPY yarn.lock .

USER web

RUN yarn install --production=true

COPY --chown=web:web . .

RUN yarn build

ENTRYPOINT ["dumb-init", "--"]

CMD yarn start

import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { useQuery, NetworkStatus } from '@apollo/client';
import Container from '@material-ui/core/Container';
import { initializeApollo } from 'lib/client';
import routes from 'lib/routes';

import Header from 'components/Header';
import Hero from 'components/Hero';
import Footer from 'components/Footer';
import PageTransition from 'components/PageTransition';
import Loader from 'components/Loader';

const QUERY = gql`
  query productQuery($slug: String!, $pageID: String!) {
    product(slug: $slug)  {
      slug
      title
      description
      content
      image
    }

    page(slug: $pageID)  {
      properties
    }

    me {
      _id
      username
    }
  }
`;

const ProductPage = ({ baseURL, variables }) => {
  const { data, error, loading, networkStatus } = useQuery(QUERY, {
    variables
  });

  if (error) return <div>{error.message}</div>;

  const loadingMore = networkStatus === NetworkStatus.fetchMore;

  if (loading || loadingMore) return <Loader/>;

  const pagePath = new URL(baseURL);

  return (
    <>
      <Head>
        <title>{ data.product.title } | { pagePath.host }</title>
        <meta name='title' content={ `${data.product.title} | ${pagePath.host}`} />
        <meta name='description' content={data.product.description } />
      </Head>

      <Header routes={routes.main}/>

      <PageTransition>
        <Container>
          <Hero title={data.product.title} content={data.product.content} image={data.product.image}/>
        </Container>
      </PageTransition>

      <Footer properties={data.page.properties}/>
    </>
  );
};

export async function getServerSideProps ({ req, params }) {
  const host = req ? req.headers['host'] : location.host;
  const proto = req ? req.headers['x-forwarded-proto'] : location.protocol;

  const variables = {
    slug: params.slug,
    pageID: 'product'
  };

  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: QUERY,
    variables,
    context: {
      headers: req.headers
    }
  });

  return {
    props: {
      baseURL: `${proto}://${host}`,
      initialApolloState: apolloClient.cache.extract(),
      variables
    }
  };
}

ProductPage.propTypes = {
  baseURL: PropTypes.string.isRequired,
  variables: PropTypes.object.isRequired,
  pageTransitionReadyToEnter: PropTypes.func
};

export default ProductPage;

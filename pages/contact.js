import React from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { useQuery, NetworkStatus } from '@apollo/client';
import { initializeApollo } from 'lib/client';
import routes from 'lib/routes';

import Template from 'templates/Home';

import Loader from 'components/Loader';

const QUERY = gql`
  query pageQuery($slug: String!) {
    page(slug: $slug)  {
      _id
      content
      slug
      title
      image
      properties
    }

    me {
      _id
      username
    }
  }
`;

const ContactPage = ({ baseURL, variables }) => {
  const { data, error, loading, networkStatus } = useQuery(QUERY, {
    variables
  });

  if (error) return <div>{error.message}</div>;

  const loadingMore = networkStatus === NetworkStatus.fetchMore;

  if (loading || loadingMore) return <Loader/>;

  return (
    <>
      <Template
        page={ data.page }
        routes={routes.main}
        baseURL={baseURL}
        footer={false}
        contactForm={true}
      />
    </>
  );
};

export async function getServerSideProps ({ req }) {
  const host = req ? req.headers['host'] : location.host;
  const proto = req ? req.headers['x-forwarded-proto'] : location.protocol;

  const variables = {
    slug: 'contact'
  };

  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: QUERY,
    variables,
    context: {
      headers: req.headers
    }
  });

  return {
    props: {
      baseURL: `${proto}://${host}`,
      initialApolloState: apolloClient.cache.extract(),
      variables
    }
  };
}

ContactPage.propTypes = {
  baseURL: PropTypes.string.isRequired,
  variables: PropTypes.object.isRequired,
  pageTransitionReadyToEnter: PropTypes.func
};

export default ContactPage;

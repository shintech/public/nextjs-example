import { ApolloServer } from 'apollo-server-micro';
import { MongoClient } from 'mongodb';
import { RedisCache } from 'apollo-server-cache-redis';
import jwt from 'jsonwebtoken';
import Cookies from 'cookies';
import schema from '../../graphql/schema';
import logger from '../../lib/logger';

const isProd = process.env['NODE_ENV'] === 'production';
const JWT_SECRET = process.env['JWT_SECRET'] || 'JWT_SECRET';
const REDIS_HOST = process.env['REDIS_HOST'] || '127.0.0.1';

let db;

const cache = new RedisCache({
  host: REDIS_HOST,
  port: 6379
});

const apolloServer = new ApolloServer({
  schema,
  debug: !isProd,
  cache,
  cacheControl: { defaultMaxAge: 5 },

  persistedQueries: {
    cache
  },

  context: async (ctx) => {
    const headers = ctx.req.headers;

    if (!db) {
      try {
        const host = process.env['MONGO_HOST'] || 'localhost';
        const port = process.env['MONGO_PORT'] || 27017;
        const user = process.env['MONGO_INITDB_ROOT_USERNAME'];
        const pass = process.env['MONGO_INITDB_ROOT_PASSWORD'];

        const dbClient = new MongoClient(
          `mongodb://${user}:${pass}@${host}:${port}?authSource=admin`,
          {
            useNewUrlParser: true,
            useUnifiedTopology: true
          }
        );

        if (!dbClient.isConnected()) await dbClient.connect();

        db = dbClient.db(process.env['MONGO_INITDB_DATABASE']);
      } catch (err) {
        logger.error('--->error while connecting with graphql context (db)');

        if (err) {
          logger.error(err);
        }
      }
    }

    const verifyToken = (token) => {
      if (!token) return null;
      try {
        return jwt.verify(token, JWT_SECRET);
      } catch {
        return null;
      }
    };

    const cookies = new Cookies(ctx.req, ctx.res);
    const token = cookies.get('auth-token');
    const user = verifyToken(token);

    return { db, headers, user, cookies };
  },

  plugins: [
    {
      requestDidStart ({ request }) {
        logger.info(`graphql: ${request.operationName || 'Undefined Operation'}...`);

        if (request.query) { logger.info(`graphql: ${request.query}`); }
        if (request.variables) { logger.info(`graphql - variables: ${JSON.stringify(request.variables, null, 2)}`); }
      }
    }
  ]
});

export const config = {
  api: {
    bodyParser: false
  }
};

export default apolloServer.createHandler({ path: '/api/graphql' });

import Head from 'next/head';
import Link from 'components/Link';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/client';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { initializeApollo } from 'lib/client';

const QUERY = gql`
  query postsQuery($slug: String!) {
    page(slug: $slug)  {
      _id
      content
      slug
      title
    }

    posts {
      _id
      title
      content
    }
  }
`;

const BlogIndex = ({ baseURL, variables }) => {
  const { data, error } = useQuery(QUERY, {
    variables
  });

  if (error) return <div>{error.message}</div>;
  if (!data) return <div>Loading...</div>;

  const pagePath = new URL(baseURL);

  const { title, content } = data.page;

  return (
    <>
      <Head>
        <title>{title.toLowerCase()} | {pagePath.host}</title>
        <meta name='title' content={ `${title} | ${pagePath.host}`} />
        <meta name='description' content={content} />
      </Head>

      <Container>
        <Typography variant="h2" component="h1">{title}!</Typography>
        <Typography variant="body1">{content}</Typography>

        <List>
          {data.posts.map(post =>
            <ListItem key={post._id}>
              <Link href='/blog/[id]' as={`/blog/${post._id}`}>
                <ListItemText primary={post.title}/>
              </Link>
            </ListItem>
          )}
        </List>
      </Container>
    </>
  );
};

export async function getServerSideProps ({ req }) {
  const host = req ? req.headers['host'] : location.host;
  const proto = req ? req.headers['x-forwarded-proto'] : location.protocol;

  const variables = {
    slug: 'blog'
  };

  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: QUERY,
    variables
  });

  return {
    props: {
      baseURL: `${proto}://${host}`,
      initialApolloState: apolloClient.cache.extract(),
      description: 'Blog Page description...',
      favicon: '/images/nodejs.svg',
      title: 'blog',
      variables
    }
  };
}

BlogIndex.propTypes = {
  baseURL: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  favicon: PropTypes.string,
  variables: PropTypes.object
};

export default BlogIndex;

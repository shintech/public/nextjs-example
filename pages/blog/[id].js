import Head from 'next/head';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/client';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { initializeApollo } from 'lib/client';

const QUERY = gql`
  query getPost($id: String!) {
    post(id: $id) {
        _id
      title
      content
    }
  }
`;

const PostPage = ({ baseURL, variables }) => {
  const { data, error } = useQuery(QUERY, {
    variables
  });

  const pagePath = new URL(baseURL);

  if (error) return <div>{error.message}</div>;
  if (!data) return <div>Loading...</div>;

  return (
    <>
      <Head>
        <title>{ data.post.title } | { pagePath.host }</title>
        <meta name='title' content={ `${data.post.title} | ${pagePath.host}`} />
        <meta name='description' content={data.post.content } />
      </Head>

      <Container>
        <Typography variant="h4" component="h1">{data.post.title}!</Typography>
        <Typography variant="body1">{data.post.content}</Typography>
      </Container>
    </>
  );
};

export async function getServerSideProps ({ req, query }) {
  const host = req ? req.headers['host'] : location.host;
  const proto = req ? req.headers['x-forwarded-proto'] : location.protocol;

  const variables = {
    id: query.id
  };

  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: QUERY,
    variables
  });

  return {
    props: {
      baseURL: `${proto}://${host}`,
      initialApolloState: apolloClient.cache.extract(),
      description: 'Posts Page description...',
      favicon: '/images/nodejs.svg',
      title: 'posts',
      variables
    }
  };
}

PostPage.propTypes = {
  baseURL: PropTypes.string,
  variables: PropTypes.object
};

export default PostPage;

/* eslint-disable react/display-name */
import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { useQuery, NetworkStatus } from '@apollo/client';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { DataGrid } from '@material-ui/data-grid';
import { initializeApollo } from 'lib/client';
import routes from 'lib/routes';

import Header from 'components/Header';
import Link from 'components/Link';
import Loader from 'components/Loader';

const QUERY = gql`
  query adminPageQuery {
    pages {
      _id
      slug
      title
      content
      image
      properties

      sections {
        title
        content
        description
        image
      }
    }

    me {
      _id
      username
    }
  }
`;

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.primary.light,
    padding: theme.spacing(2),
    marginTop: theme.spacing(2),
    borderRadius: '5px',

    '& .app-theme-header': {
      backgroundColor: theme.palette.primary.main,
      color: 'white',

      '&:last-child .MuiDataGrid-columnSeparator': {
        display: 'none'
      }
    },

    '& .MuiDataGrid-row': {
      backgroundColor: theme.palette.primary.dark,

      '&:hover': {
        backgroundColor: theme.palette.info.main
      }
    },

    '& .MuiDataGrid-row.Mui-odd': {
      backgroundColor: theme.palette.primary.light
    },

    '& .MuiDataGrid-footer': {
      backgroundColor: theme.palette.primary.main
    }
  }
}));

const HomePage = ({ baseURL, variables }) => {
  const classes = useStyles();

  const { data, error, loading, networkStatus } = useQuery(QUERY, {
    variables
  });

  if (error) return <div>{error.message}</div>;

  const loadingMore = networkStatus === NetworkStatus.fetchMore;

  if (loading || loadingMore) return <Loader/>;

  const pagePath = new URL(baseURL);

  const columns = [
    { field: '_id',
      headerName: 'Edit',
      headerClassName: 'app-theme-header',
      width: 200,
      renderCell: params =>
        <div>
          <Button
            variant="contained"
            size="small"
            component={Link}
            href={`/admin/pages/${encodeURIComponent(params.row.slug)}/edit`}
          >
            Open
          </Button>
        </div>

    },
    {
      field: 'slug',
      headerName: 'Slug',
      headerClassName: 'app-theme-header',
      width: 200
    },
    {
      field: 'title',
      headerName: 'Title',
      headerClassName: 'app-theme-header',
      flex: 1
    },
    {
      field: 'sections',
      headerName: 'Sections',
      headerClassName: 'app-theme-header',
      type: 'number',
      width: 300,
      valueGetter: params => params.row?.sections?.length
    }
  ];

  if (!data.me) {
    return <Typography className='header' component="h1" variant="h1" color="textPrimary" gutterBottom>Unauthorized</Typography>;
  }

  return (
    <>
      <Head>
        <title>Edit Pages | { pagePath.host }</title>
        <meta name='title' content={ `Admin | ${pagePath.host}`} />
      </Head>

      <Header routes={routes.admin}/>

      <Container className={classes.root}>
        <fieldset>
          <legend>
            <Typography className='header' component="h4" variant="h5" gutterBottom>Edit Pages</Typography>
          </legend>
          <div>
            <DataGrid
              getRowId={(row) => row._id}
              rows={data.pages}
              columns={columns}
              pageSize={13}
              autoPageSize
              autoHeight
              disableSelectionOnClick
            />
          </div>
        </fieldset>
      </Container>
    </>
  );
};

export async function getServerSideProps ({ req }) {
  const host = req ? req.headers['host'] : location.host;
  const proto = req ? req.headers['x-forwarded-proto'] : location.protocol;

  const variables = {
    slug: 'home'
  };

  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: QUERY,
    variables,
    context: {
      headers: req.headers
    }
  });

  return {
    props: {
      baseURL: `${proto}://${host}`,
      initialApolloState: apolloClient.cache.extract(),
      variables
    }
  };
};

HomePage.propTypes = {
  baseURL: PropTypes.string.isRequired,
  variables: PropTypes.object.isRequired,
  pageTransitionReadyToEnter: PropTypes.func
};

export default HomePage;

import React, { useState } from 'react';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { gql, useMutation, useQuery, NetworkStatus, useReactiveVar } from '@apollo/client';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import green from '@material-ui/core/colors/green';

import { initializeApollo } from 'lib/client';
import routes from 'lib/routes';
import { snackbarVar } from 'lib/client/vars';

import Header from 'components/Header';
import Loader from 'components/Loader';

import Template from 'templates/Home';

const RenderInWindow = dynamic(
  () => import('components/RenderInWindow'),
  { ssr: false }
);

const QUERY = gql`
  query editPageQuery($slug: String!) {
    page(slug: $slug)  {
      _id
      content
      slug
      image
      title
      properties

      sections {
        title
        content
        description
        image
      }

      products {
        slug
        title
        description
      }
    }

    files {
      filename
    }

    me {
      _id
      username
    }
  }
`;

const MUTATION = gql`
  mutation pageMutation($title: String, $content: String, $image: String, $slug: String) {
    page(title: $title, content: $content, image: $image, slug: $slug) {
      title
      content
      image
      slug
    }
  }
`;

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    '& fieldset': {
      borderRadius: '5px'
    },

    '& button:last-child': {
      marginLeft: theme.spacing(2)
    }
  },

  form: {
    backgroundColor: 'lightgrey',
    padding: theme.spacing(2),
    marginTop: theme.spacing(2),
    border: '1px solid white',
    borderRadius: '5px',

    '& .wrapper': {
      margin: theme.spacing(1),
      position: 'relative'
    },

    '& .MuiFormHelperText-root.Mui-error::first-letter': {
      textTransform: 'capitalize'
    }
  },

  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '4.5%',
    marginTop: -12,
    marginLeft: -12
  },

  input: {
    backgroundColor: 'white',
    color: 'black'
  },

  textField: {
    '& .MuiFilledInput-root': {
      color: 'black',
      backgroundColor: 'rgb(232, 241, 250)'
    },
    '& .MuiFilledInput-root:hover': {
      backgroundColor: 'rgb(250, 232, 241)',
      // Reset on touch devices, it doesn't add specificity
      '@media (hover: none)': {
        backgroundColor: 'rgb(232, 241, 250)'
      }
    },
    '& .MuiFilledInput-root.Mui-focused': {
      backgroundColor: 'rgb(250, 241, 232)'
    },

    '& .MuiInputLabel-root': {
      color: 'black'
    }
  },

  select: {
    marginBottom: theme.spacing(2),
    color: 'red',

    '& .MuiInputBase-root, & .MuiInputLabel-root': {
      color: theme.palette.primary.main
    },

    '& .MuiInputLabel-root': {
      color: 'black'
    }
  },

  buttons: {
    display: 'flex',

    '& > a': {
      minWidth: theme.spacing(20),

      '&:first-child': {
        marginRight: theme.spacing(1)
      }
    }
  },

  markdown: {
    fontSize: '16px'
  }
}));

const HomePage = ({ baseURL, variables, headers }) => {
  const classes = useStyles();
  const router = useRouter();
  const snackbar = useReactiveVar(snackbarVar);

  const { data, error, loading, networkStatus } = useQuery(QUERY, {
    variables,
    context: {
      headers
    }
  });

  const update = (cache, { data }) => {
    snackbarVar({
      open: true,
      message: `Successfully updated page: ${data.page.slug}`,
      severity: 'success'
    });
  };

  const [mutate, {
    loading: mutationLoading, // eslint-disable-line,
    data: mutationData // eslint-disable-line
  }] = useMutation(MUTATION, { update });

  const [popOut, setPopOut] = useState({
    open: false
  });

  if (error) return <div>{error.message}</div>;

  const loadingMore = networkStatus === NetworkStatus.fetchMore;

  if (loading || loadingMore) return <Loader/>;

  const pagePath = new URL(baseURL);

  const [values, setValues] = useState(() => {
    if (data && data.page) {
      return {
        content: data.page.content,
        image: data.page.image,
        slug: data.page.slug,
        title: data.page.title
      };
    } else {
      return {};
    }
  });

  const handleChange = e => {
    const { name, value: newValue, type } = e.target;

    // keep number fields as numbers
    const value = type === 'number' ? +newValue : newValue;

    // save field values
    setValues({
      ...values,
      [name]: value
    });
  };

  const handleSubmit = e => {
    e.preventDefault();

    // eslint-disable-next-line no-console
    console.log('submitted', values);

    mutate({
      variables: {
        title: values.title,
        content: values.content,
        image: values.image,
        slug: values.slug
      },
      context: {
        headers
      }
    });
  };

  const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    snackbarVar({
      open: false,
      message: '',
      severity: 'info'
    });
  };

  if (!data.me) {
    return <Typography className='header' component="h1" variant="h1" color="textPrimary" gutterBottom>Unauthorized</Typography>;
  }

  return (
    <>
      <Head>
        <title>Admin | { pagePath.host }</title>
        <meta name='title' content={ `Admin | ${pagePath.host}`} />
      </Head>

      <Header routes={routes.admin} disable={popOut.open}/>

      <Container maxWidth='xl' className={classes.root}>
        <div className={classes.preview}>
          <fieldset>
            <legend>
              <Button
                variant='contained'
                color='primary'
                onClick={() => router.back()}
                startIcon={<ArrowBackIcon/>}
                disabled={popOut.open}
              >
                Back
              </Button>

              <Button
                variant='contained'
                color='primary'
                onClick={() => setPopOut({ open: !popOut.open })}
              >
                {popOut.open ? 'Close' : 'Edit'}
              </Button>
            </legend>

            <Template
              page={{
                ...data.page,
                title: values.title,
                content: values.content,
                image: values.image
              }}
              routes={routes.admin}
              baseURL={baseURL}
              footer={data.page.slug !== 'contact'}
              header={false}
              preview={true}
            />
          </fieldset>
        </div>
      </Container>

      <Snackbar open={snackbar.open} autoHideDuration={10000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={snackbar.severity}>
          {snackbar.message}
        </Alert>
      </Snackbar>

      {
        popOut.open && <RenderInWindow title={`Editing Page: ${data.page.title}`} closeWindow={() => setPopOut({ open: false })}>
          <form className={classes.form} onSubmit={handleSubmit}>
            <fieldset>
              <legend>
                <Typography className='header' component="h3" variant="h4" color="primary" gutterBottom>Editing Page: { data.page.title }</Typography>
              </legend>

              <TextField
                fullWidth
                variant="filled"
                label="Title"
                name="title"
                value={values.title}
                onChange={handleChange}
                className={classes.textField}
                helperText=' '
                InputProps={{
                  className: classes.input
                }}
              />

              <TextField
                fullWidth
                variant="filled"
                label="Slug"
                name="slug"
                value={values.slug}
                onChange={handleChange}
                className={classes.textField}
                helperText=' '
                InputProps={{
                  className: classes.input
                }}
              />

              <FormControl className={classes.select}>
                <InputLabel id="file-select-label">Image</InputLabel>
                <Select
                  labelId="file-select-label"
                  id="file-select"
                  name="image"
                  value={values.image}
                  onChange={handleChange}
                >
                  {data.files.map(({ filename }) => <MenuItem key={filename} value={filename}>{filename}</MenuItem>)}
                </Select>
              </FormControl>

              <TextField
                fullWidth
                multiline
                rows={20}
                variant="filled"
                label="Content"
                name="content"
                value={values.content}
                onChange={handleChange}
                className={classes.textField}
                helperText=' '
                InputProps={{
                  className: classes.input
                }}
              />

              <div className='wrapper'>
                <Button type='submit' variant='contained' size='large'>
                  Submit
                </Button>

                {mutationLoading && <CircularProgress size={24} className={classes.buttonProgress} />}
              </div>
            </fieldset>
          </form>
        </RenderInWindow>
      }
    </>
  );
};

export async function getServerSideProps ({ req, params }) {
  const host = req ? req.headers['host'] : location.host;
  const proto = req ? req.headers['x-forwarded-proto'] : location.protocol;

  const variables = {
    slug: params.slug
  };

  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: QUERY,
    variables,
    context: {
      headers: req.headers
    }
  });

  return {
    props: {
      baseURL: `${proto}://${host}`,
      initialApolloState: apolloClient.cache.extract(),
      variables,
      headers: req.headers
    }
  };
};

HomePage.propTypes = {
  baseURL: PropTypes.string.isRequired,
  variables: PropTypes.object.isRequired,
  pageTransitionReadyToEnter: PropTypes.func,
  headers: PropTypes.object
};

export default HomePage;

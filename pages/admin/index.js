/* eslint-disable react/display-name */
import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { useQuery, NetworkStatus } from '@apollo/client';
// import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import { initializeApollo } from 'lib/client';
import routes from 'lib/routes';

import Link from 'components/Link';
import Loader from 'components/Loader';
import Template from 'templates/Home';

const QUERY = gql`
  query adminPageQuery($slug: String!) {
    pages {
      _id
      slug
      title
      content
      image
      properties

      sections {
        title
        content
        description
        image
      }
    }

    page(slug: $slug)  {
      content
      slug
      title
      image
      properties
    }

    me {
      _id
      username
    }
  }
`;

// const useStyles = makeStyles((theme) => ({}));

const HomePage = ({ baseURL, variables }) => {
  // const classes = useStyles();

  const { data, error, loading, networkStatus } = useQuery(QUERY, {
    variables
  });

  if (error) return <div>{error.message}</div>;

  const loadingMore = networkStatus === NetworkStatus.fetchMore;

  if (loading || loadingMore) return <Loader/>;

  const pagePath = new URL(baseURL);

  if (!data.me) {
    return <Typography className='header' component="h1" variant="h1" color="textPrimary" gutterBottom>Unauthorized</Typography>;
  }

  return (
    <>
      <Head>
        <title>Admin | { pagePath.host }</title>
        <meta name='title' content={ `Admin | ${pagePath.host}`} />
      </Head>

      <Template
        page={ data.page }
        routes={routes.admin}
        baseURL={baseURL}
        hero={false}
        footer={false}
      >
        <Typography className='header' component="h1" variant="h2" color="textPrimary" gutterBottom>{ data.page.title }</Typography>

        <List>
          <ListItem component={Link} href='/admin/pages' button>
            <ListItemText primary='Pages' />
          </ListItem>
        </List>
      </Template>
    </>
  );
};

export async function getServerSideProps ({ req }) {
  const host = req ? req.headers['host'] : location.host;
  const proto = req ? req.headers['x-forwarded-proto'] : location.protocol;

  const variables = {
    slug: 'admin'
  };

  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: QUERY,
    variables,
    context: {
      headers: req.headers
    }
  });

  return {
    props: {
      baseURL: `${proto}://${host}`,
      initialApolloState: apolloClient.cache.extract(),
      variables
    }
  };
};

HomePage.propTypes = {
  baseURL: PropTypes.string.isRequired,
  variables: PropTypes.object.isRequired,
  pageTransitionReadyToEnter: PropTypes.func
};

export default HomePage;

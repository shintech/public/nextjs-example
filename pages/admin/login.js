import React, { useState, createRef } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { gql, useMutation, useQuery, NetworkStatus } from '@apollo/client';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import ReCAPTCHA from 'react-google-recaptcha';
import green from '@material-ui/core/colors/green';

import { initializeApollo } from 'lib/client';
import validate from 'lib/validation';
import routes from 'lib/routes';

import Loader from 'components/Loader';
import Template from 'templates/Home';

import { snackbarVar } from 'lib/client/vars';

const recaptchaSiteKey = (process.env['NODE_ENV'] !== 'production') ? '6LesxbYUAAAAANA_Si5ylzSGwgbX8O7uECnPZ_p8' : '6LfvffgZAAAAAOW13Qs7X6-dlXZASi4wokSYRWye';

const recaptchaRef = createRef();

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },

  form: {
    color: 'black',
    border: '1px solid white',
    borderRadius: '5px',
    padding: theme.spacing(2),
    textAlign: 'center',
    backgroundColor: 'darkslategrey',

    '& .wrapper': {
      margin: theme.spacing(1),
      position: 'relative'
    },

    '& .MuiFormHelperText-root.Mui-error::first-letter': {
      textTransform: 'capitalize'
    }
  },

  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  }
}));

const initialState = {
  username: '',
  password: ''
};

const QUERY = gql`
  query adminPageQuery($slug: String!) {
    page(slug: $slug)  {
      content
      slug
      title
      image
      properties
    }

    me {
      _id
      username
    }
  }
`;

const MUTATION = gql`
  mutation authenticateMutation($username: String!, $password: String!) {
    authenticate(username: $username, password: $password) {
      status
      message

      user {
        username
        token
      }

    }
  }
`;

const LoginForm = ({ baseURL, variables }) => {
  const [values, setValues] = useState(initialState);
  const [errors, setErrors] = useState({});
  const [touched, setTouched] = useState({});

  const classes = useStyles();
  const router = useRouter();

  const update = (cache, { data }) => {
    snackbarVar({
      open: true,
      message: data.authenticate.message,
      severity: data.authenticate.status
    });
  };

  const [mutate, {
    loading: mutationLoading, // eslint-disable-line
    error: mutationError, // eslint-disable-line
    data: mutationData
  }] = useMutation(MUTATION, { update });

  const handleChange = e => {
    const { name, value: newValue, type } = e.target;

    // keep number fields as numbers
    const value = type === 'number' ? +newValue : newValue;

    // save field values
    setValues({
      ...values,
      [name]: value
    });

    // was the field modified
    setTouched({
      ...touched,
      [name]: true
    });
  };

  const handleBlur = e => {
    const { name, value } = e.target;

    // remove whatever error was there previously
    const { [name]: removedError, ...rest } = errors;

    // check for a new error
    const error = validate(name, value);

    // // validate the field if the value has been touched
    setErrors({
      ...rest,
      ...(error && { [name]: touched[name] && error })
    });
  };

  const handleSubmit = async e => {
    e.preventDefault();
    // validate the form
    const formValidation = Object.keys(values).reduce(
      (acc, key) => {
        // const newError = validate[key](values[key]);
        const newError = validate(key, values[key]);
        const newTouched = { [key]: true };

        return {
          errors: {
            ...acc.errors,
            ...(newError && { [key]: newError })
          },
          touched: {
            ...acc.touched,
            ...newTouched
          }
        };
      },
      {
        errors: { ...errors },
        touched: { ...touched }
      }
    );

    setErrors(formValidation.errors);
    setTouched(formValidation.touched);

    if (
      !Object.values(formValidation.errors).length && // errors object is empty
      Object.values(formValidation.touched).length === Object.values(values).length && // all fields were touched
      Object.values(formValidation.touched).every(t => t === true) // every touched field is true
    ) {
      try {
        const token = await recaptchaRef.current.executeAsync();

        recaptchaRef.current.reset();

        await mutate({
          variables: values,

          context: {
            headers: {
              'g-recaptcha-response': token
            }
          }
        });

        setValues(initialState);
      } catch (err) {
        console.error(err.message); // eslint-disable-line
      }
    }
  };

  const { data: queryData, error, loading, networkStatus } = useQuery(QUERY, {
    variables
  });

  if (error) return <div>{error.message}</div>;

  const loadingMore = networkStatus === NetworkStatus.fetchMore;

  if (loading || loadingMore) return <Loader/>;

  if (mutationData && mutationData?.authenticate?.user?.token) {
    router.push('/admin');
  }

  return (
    <>
      <Head>
        <title>Login</title>
      </Head>

      <Template
        page={ queryData.page }
        routes={routes.admin}
        baseURL={baseURL}
        hero={false}
        header={false}
        footer={false}
      >
        <Container className={classes.root}>
          <form className={classes.form} onSubmit={handleSubmit}>
            <TextField
              fullWidth
              type='text'
              variant="filled"
              label="Username"
              name="username"
              onChange={handleChange}
              value={values.username || ''}
              onBlur={handleBlur}
              error={!!errors.username}
              helperText={(errors.username) ? errors.username : ' '}
            />

            <TextField
              fullWidth
              type="password"
              variant="filled"
              label="Password"
              name="password"
              onChange={handleChange}
              value={values.password || ''}
              onBlur={handleBlur}
              error={!!errors.password}
              helperText={(errors.password) ? errors.password : ' '}
            />

            <ReCAPTCHA
              theme='dark'
              sitekey={recaptchaSiteKey}
              ref={recaptchaRef}
              size="invisible"
            />

            <div className='wrapper'>
              <Button type='submit' variant='contained' size='large' disabled={mutationLoading} >
              Submit
              </Button>

              {mutationLoading && <CircularProgress size={24} className={classes.buttonProgress} />}
            </div>
          </form>
        </Container>
      </Template>
    </>
  );
};

export async function getServerSideProps ({ req }) {
  const host = req ? req.headers['host'] : location.host;
  const proto = req ? req.headers['x-forwarded-proto'] : location.protocol;

  const variables = {
    slug: 'admin'
  };

  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: QUERY,
    variables,
    context: {
      headers: req.headers
    }
  });

  return {
    props: {
      baseURL: `${proto}://${host}`,
      initialApolloState: apolloClient.cache.extract(),
      variables
    }
  };
};

LoginForm.propTypes = {
  variables: PropTypes.object,
  baseURL: PropTypes.string
};

LoginForm.defaultProps = {
  baseURL: PropTypes.string.isRequired,
  variables: PropTypes.object.isRequired
};

export default LoginForm;

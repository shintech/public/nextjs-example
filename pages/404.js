import Image from 'next/image';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/client';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

import Footer from 'components/Footer';
import PageTransition from 'components/PageTransition';
import Loader from 'components/Loader';

const QUERY = gql`
  query pageQuery($slug: String!) {
    page(slug: $slug)  {
      properties
    }
  }
`;

const useStyles = makeStyles((theme) => ({
  root: {
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center'
  }
}));

export default function Custom404 () {
  const classes = useStyles();

  const { data, error, loading } = useQuery(QUERY, {
    variables: {
      slug: 'notfound'
    }
  });

  if (error) return <div>{error.message}</div>;
  if (!data || loading) return <Loader/>;

  return (
    <>
          <PageTransition>

            <Container className={classes.root}>
              <Typography className='header' component="h1" variant="h2" color="textPrimary" gutterBottom>Not Found...</Typography>

              <Image loading='eager' priority={true} layout='responsive' width='100%' height='50%' src={`/images/404.svg`}/>
            </Container>

            <Footer properties={data.page.properties}/>
          </PageTransition>

    </>
  );
}

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { ApolloProvider } from '@apollo/client';
import { ThemeProvider, createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import { CacheProvider } from '@emotion/react';
import CssBaseline from '@material-ui/core/CssBaseline';
import createCache from '@emotion/cache';
import { AnimateSharedLayout } from 'framer-motion';

import grey from '@material-ui/core/colors/grey';
import red from '@material-ui/core/colors/red';
import orange from '@material-ui/core/colors/orange';
import blue from '@material-ui/core/colors/blue';
import teal from '@material-ui/core/colors/teal';
import green from '@material-ui/core/colors/green';
import brown from '@material-ui/core/colors/brown';

import { useApollo } from 'lib/client';

export const cache = createCache({ key: 'css' });

export let theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: grey[900],
      dark: grey[700],
      light: grey[600],
      contrastText: teal[200]
    },
    secondary: {
      main: brown[500],
      dark: brown[800],
      light: brown[400],
      contrastText: teal[200]
    },
    error: {
      main: red[500]
    },
    warning: {
      main: orange[500]
    },
    info: {
      main: blue[700]
    },
    success: {
      main: green[500]
    },
    background: {
      default: '#585858'
    }
  },

  spacing: factor => `${0.5 * factor}rem`,

  overrides: {
    MuiCssBaseline: {
      '@global': {
        body: {
          background: `
            background-color: #585858;
            background-image: url("data:image/svg+xml,%3Csvg width='6' height='6' viewBox='0 0 6 6' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23000000' fill-opacity='0.4' fill-rule='evenodd'%3E%3Cpath d='M5 0h1L0 6V5zM6 5v1H5z'/%3E%3C/g%3E%3C/svg%3E");
          `,
          backgroundAttachment: 'fixed'
        }
      }
    }
  },

  typography: {
    fontFamily: [
      'Times New Roman',
      'Times',
      'serif'
    ].join(',')
  }
});

theme = responsiveFontSizes(theme);

export default function MyApp ({ Component, pageProps, router, siteName }) {
  const apolloClient = useApollo(pageProps.initialApolloState);

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <CacheProvider value={cache}>
      <ApolloProvider client={apolloClient}>
        <Head>
          <title>{pageProps.siteName || siteName} | { pageProps.baseURL || router.pathname }</title>
          <meta name="viewport" content="initial-scale=1, width=device-width" />
        </Head>

        <ThemeProvider theme={theme}>
          <CssBaseline />
          <AnimateSharedLayout>
            <Component {...pageProps} />
          </AnimateSharedLayout>
        </ThemeProvider>
      </ApolloProvider>
    </CacheProvider>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
  siteName: PropTypes.string,
  router: PropTypes.object
};

MyApp.defaultProps = {
  siteName: 'Tulsa Tech Services'
};
